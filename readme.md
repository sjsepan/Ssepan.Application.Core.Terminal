# readme.md - README for Ssepan.Application.Core.Terminal

## About

Common library of application functions for C# .Net Core applications, specific to console UIs; requires ssepan.application.core, ssepan.io.core, ssepan.utility.core

### Purpose

To encapsulate common application functionality, reduce custom coding needed to start a new project, and provide consistency across projects.

### Usage notes

~...

### History

6.0:
~update to net8.0
~refactor to newer C# features
~refactor to existing language types
~perform format linting

5.1:
~Add missing AssemblyInfo.cs under properties.
~Use new Website property in AssemblyInfoBase, and set in AssemblyInfo for modules that will display About.

5.0:
~Refactor Ssepan.Application.Core.GtkSharp into several libraries. Moved classes specific to Console UI into Ssepan.Application.Core.Terminal. (Note that this does not apply to the classes used by all programs in Program.cs to start up, including command-line argument parsing and such).

Steve Sepan
<sjsepan@yahoo.com>
2/28/2024
