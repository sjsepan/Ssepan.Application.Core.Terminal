﻿using System;
using System.IO;
using Ssepan.Io.Core;
using Ssepan.Utility.Core;
//using Microsoft.Data.ConnectionUI;//cannot use until Microsoft.VisualStudio.Data.dll is made re-distributable by Microsoft
using System.Reflection;

namespace Ssepan.Application.Core.Terminal
{
    public static class Dialogs
    {
        #region Declarations
        public const string FilterSeparator = "|";
        public const string FilterDescription = "{0} Files(s)";
        public const string FilterFormat = "{0} (*.{1})|*.{1}";
        #endregion Declarations

        #region Methods
        /// <summary>
        /// Get path to save data.
        /// Static.
        /// </summary>
        /// <param name="fileDialogInfo">ref FileDialogInfo<object, string></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetPathForSave
        (
            ref FileDialogInfo<object, string> fileDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			const string messageResponse = null;
			try
			{
                if
                (
                    fileDialogInfo.Filename.EndsWith(fileDialogInfo.NewFilename)
                    ||
                    fileDialogInfo.ForceDialog
                )
                {
                    Console.WriteLine(fileDialogInfo.ForceDialog ? "Save As..." : "Save...");
                    Console.WriteLine("Current filename: {0}", fileDialogInfo.Filename);
                    Console.WriteLine("Filters...");
                    foreach (string filter in fileDialogInfo.Filters.Split(","))
                    {
						string[] nameAndPattern = filter.Split("|");
						Console.WriteLine("Name: {0}, Pattern: {1}", nameAndPattern[0], nameAndPattern[1]);
                    }
					string currentPath =
                        fileDialogInfo.InitialDirectory == default
                        ?
                        fileDialogInfo.CustomInitialDirectory
                        :
                        Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
                        ;

					Console.WriteLine("Current folder: {0}", currentPath);

                    Console.Write("Filename?: ");
					string fileResponse = Console.ReadLine();

					if (!string.IsNullOrWhiteSpace(fileResponse))
                    {
                        if (string.Equals(Path.GetFileName(fileResponse), fileDialogInfo.NewFilename + "." + fileDialogInfo.Extension, StringComparison.CurrentCultureIgnoreCase))
                        {
							//user did not select or enter a name different than new; for now I have chosen not to allow that name to be used for a file.--SJS, 12/16/2005
							string messageTemp = "The name \"" + (fileDialogInfo.NewFilename + "." + fileDialogInfo.Extension).ToLower() + "\" is not allowed; please choose another. Settings not saved.";
							MessageDialogInfo<object, string, object, string, object> messageDialogInfo =
                                new (
                                    fileDialogInfo.Parent,
                                    fileDialogInfo.Modal,
                                    fileDialogInfo.Title,
                                    null,
                                    "Info",
                                    null,
                                    messageTemp,
                                    messageResponse
                                );
                            ShowMessageDialog
                            (
                                ref messageDialogInfo,
                                ref errorMessage
                            );
                        }
                        else
                        {
                            //set new filename
                            fileDialogInfo.Filename = fileResponse;
                            fileDialogInfo.Filenames = [fileResponse];
                        }
                        fileDialogInfo.Response = fileResponse;
                        returnValue = true;
                    }
                }
                else
                {
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }

            return returnValue;
        }

        /// <summary>
        /// Get path to load data.
        /// Static.
        /// </summary>
        /// <param name="fileDialogInfo">ref FileDialogInfo<object, string></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetPathForLoad
        (
            ref FileDialogInfo<object, string> fileDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			const string messageResponse = null;
			try
			{
                if (fileDialogInfo.ForceNew)
                {
                    fileDialogInfo.Filename = fileDialogInfo.NewFilename;

                    returnValue = true;
                }
                else
                {
                    string PreviousFileName = fileDialogInfo.Filename;

                    //define location of file for settings by prompting user for filename.
                    Console.WriteLine("Open...");
                    Console.WriteLine("Current filename: {0}", fileDialogInfo.Filename);
                    Console.WriteLine("Filters...");
                    foreach (string filter in fileDialogInfo.Filters.Split(","))
                    {
						string[] nameAndPattern = filter.Split("|");
						Console.WriteLine("Name: {0}, Pattern: {1}", nameAndPattern[0],  nameAndPattern[1]);
                    }
					string currentPath =
                        fileDialogInfo.InitialDirectory == default
                        ?
                        fileDialogInfo.CustomInitialDirectory
                        :
                        Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator();
					Console.WriteLine("Current folder: {0}", currentPath);

                    Console.Write("Filename?: ");
					string fileResponse = Console.ReadLine();

					if (!string.IsNullOrWhiteSpace(fileResponse))
                    {
                        if (string.Equals(Path.GetFileName(fileResponse), fileDialogInfo.NewFilename + "." + fileDialogInfo.Extension, StringComparison.CurrentCultureIgnoreCase))
                        {
							//user did not select or enter a name different than new; for now I have chosen not to allow that name to be used for a file.--SJS, 12/16/2005
							string messageTemp = "The name \"" + (fileDialogInfo.NewFilename + "." + fileDialogInfo.Extension).ToLower() + "\" is not allowed; please choose another. Settings not opened.";
							MessageDialogInfo<object, string, object, string, object> messageDialogInfo =
                                new                                (
                                    fileDialogInfo.Parent,
                                    fileDialogInfo.Modal,
                                    fileDialogInfo.Title,
                                    null,
                                    "Info",
                                    null,
                                    messageTemp,
                                    messageResponse
                                );
                            ShowMessageDialog
                            (
                                ref messageDialogInfo,
                                ref errorMessage
                            );
                        }
                        else
                        {
                            //set new filename
                            fileDialogInfo.Filename = fileResponse;
                            fileDialogInfo.Filenames = [fileResponse];
                        }
                        fileDialogInfo.Response = fileResponse;
                        returnValue = true;
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }

            return returnValue;
        }

        /// <summary>
        /// Select a folder path.
        /// Static.
        /// </summary>
        /// <param name="fileDialogInfo">ref FileDialogInfo<object, string>. returns path in Filename property</param>
        /// <param name="errorMessage">ref string. </param>
        /// <returns>bool</returns>
        public static bool GetFolderPath
        (
            ref FileDialogInfo<object, string> fileDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			try
			{
                Console.WriteLine("Open folder...");
				string currentPath =
                        fileDialogInfo.InitialDirectory == default
                        ?
                        fileDialogInfo.CustomInitialDirectory
                        :
                        Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
                    ;

				Console.WriteLine("Current folder: {0}", currentPath);

                Console.Write("Folder?: ");
				string fileResponse = Console.ReadLine();

				if (!string.IsNullOrWhiteSpace(fileResponse))
                {
                    fileDialogInfo.Filename = fileResponse;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        /// <summary>
        /// Shows an About dialog.
        /// Static.
        /// </summary>
        /// <param name="aboutDialogInfo">ref AboutDialogInfo<object, string, object></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool ShowAboutDialog
        (
            ref AboutDialogInfo<object, string, object> aboutDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			try
			{
                Console.WriteLine(aboutDialogInfo.Title,": ");
                Console.WriteLine("ProgramName: {0}", aboutDialogInfo.ProgramName);
                Console.WriteLine("Version: {0}: ", aboutDialogInfo.Version);
                Console.WriteLine("Copyright: {0}", aboutDialogInfo.Copyright);
                Console.WriteLine("Comments: {0}", aboutDialogInfo.Comments);
                Console.WriteLine("Website: {0}", aboutDialogInfo.Website);
                Console.Write("Press ENTER to Continue...");
				string response = Console.ReadLine();
				if (!string.IsNullOrWhiteSpace(response))
                {
                    aboutDialogInfo.Response = response;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        /// <summary>
        /// Get a printer.
        /// Static.
        /// </summary>
        /// <param name="printerDialogInfo">ref PrinterDialogInfo<object, string, string>. PrinterDialogInfo</param>
        /// <param name="errorMessage">ref string. </param>
        /// <returns>bool</returns>
        public static bool GetPrinter
        (
            ref PrinterDialogInfo<object, string, string> printerDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			try
			{
                Console.Write(printerDialogInfo.Title,": ");
				//TODO:list available printers
				string printerResponse = Console.ReadLine();
				if (!string.IsNullOrWhiteSpace(printerResponse))
                {
                    printerDialogInfo.Name = printerResponse;
                    printerDialogInfo.Response = "OK";
                    returnValue = true;
                }
                else
                {
                    printerDialogInfo.Response = "Cancel";
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }
        /// <summary>
        /// Shows a message dialog.
        /// Static.
        /// </summary>
        /// <param name="messageDialogInfo">ref MessageDialogInfo<object, string, object, string, object></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool ShowMessageDialog
        (
            ref MessageDialogInfo<object, string, object, string, object> messageDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			try
			{
                Console.Write(messageDialogInfo.MessageType, "; ", messageDialogInfo.Title,": ");
				string response = Console.ReadLine();
				if (!string.IsNullOrWhiteSpace(response))
                {
                    messageDialogInfo.Response = response;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        /// <summary>
        /// Get a color.
        /// Static.
        /// </summary>
        /// <param name="colorDialogInfo">ref ColorDialogInfo<object, string, string>. </param>
        /// <param name="errorMessage">ref string. </param>
        /// <returns>bool</returns>
        public static bool GetColor
        (
            ref ColorDialogInfo<object, string, string> colorDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			try
			{
                Console.Write(colorDialogInfo.Title,": ");
				string response = Console.ReadLine();
				if (!string.IsNullOrWhiteSpace(response))
                {
                    colorDialogInfo.Color = response;
                    colorDialogInfo.Response = response;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.WriteLine(ex.Message);
            }

            return returnValue;
        }

        /// <summary>
        /// Get a font descriptor.
        /// Static.
        /// </summary>
        /// <param name="fontDialogInfo">ref FontDialogInfo<object, string, string></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetFont
        (
            ref FontDialogInfo<object, string, string> fontDialogInfo,//Pango.FontDescription fontDescription,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			try
			{
                Console.Write(fontDialogInfo.Title,": ");
				string response = Console.ReadLine();
				if (!string.IsNullOrWhiteSpace(response))
                {
                    fontDialogInfo.FontDescription = response;
                    fontDialogInfo.Response = response;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.WriteLine(ex.Message);
            }

            return returnValue;
        }

        // /// <summary>
        // /// Perform input of connection string and provider name.
        // /// Uses MS Data Connections Dialog.
        // /// Note: relies on MS-LPL license and code from http://archive.msdn.microsoft.com/Connection
        // /// </summary>
        // /// <param name="connectionString"></param>
        // /// <param name="providerName"></param>
        // /// <param name="errorMessage"></param>
        // /// <returns></returns>
        // public static bool GetDataConnection
        // (
        //     ref string connectionString,
        //     ref string providerName,
        //     ref string errorMessage
        // )
        // {
        //     bool returnValue = default;
        //     //DataConnectionDialog dataConnectionDialog = default(DataConnectionDialog);
        //     //DataConnectionConfiguration dataConnectionConfiguration = default(DataConnectionConfiguration);

        //     try
        //     {
        //         //dataConnectionDialog = new DataConnectionDialog();

        //         //DataSource.AddStandardDataSources(dataConnectionDialog);

        //         //dataConnectionDialog.SelectedDataSource = DataSource.SqlDataSource;
        //         //dataConnectionDialog.SelectedDataProvider = DataProvider.SqlDataProvider;//TODO:use?

        //         //dataConnectionConfiguration = new DataConnectionConfiguration(null);
        //         //dataConnectionConfiguration.LoadConfiguration(dataConnectionDialog);

        //         //(don't) set to current connection string, because it overwrites previous settings, requiring user to click Refresh in Data Connection Dialog.
        //         //dataConnectionDialog.ConnectionString = connectionString;

        //         if (true/*DataConnectionDialog.Show(dataConnectionDialog) == DialogResult.OK*/)
        //         {
        //             ////extract connection string
        //             //connectionString = dataConnectionDialog.ConnectionString;
        //             //providerName = dataConnectionDialog.SelectedDataProvider.ViewName;

        //             ////writes provider selection to xml file
        //             //dataConnectionConfiguration.SaveConfiguration(dataConnectionDialog);

        //             ////save these too
        //             //dataConnectionConfiguration.SaveSelectedProvider(dataConnectionDialog.SelectedDataProvider.ToString());
        //             //dataConnectionConfiguration.SaveSelectedSource(dataConnectionDialog.SelectedDataSource.ToString());

        //             returnValue = true;
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         errorMessage = ex.Message;
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

        //     }
        //     return returnValue;
        // }
        #endregion Methods
    }
}
