﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using Ssepan.Utility.Core;
// using Ssepan.Application.Core;

namespace Ssepan.Application.Core.Terminal
{
    /// <summary>
    /// Note: this class can be subclassed without type parameters in the client.
    /// </summary>
    /// <typeparam name="TIcon">TIcon</typeparam>
    /// <typeparam name="TSettings">TSettings</typeparam>
    /// <typeparam name="TModel">TModel</typeparam>
    public class ConsoleViewModel
    <
        TIcon,
        TSettings,
        TModel
    > :
        ViewModelBase<TIcon>
        where TIcon : class
        where TSettings : class, ISettings, new()
        where TModel : class, IModel, new()
    {
        #region Declarations
        //public delegate bool DoWork_WorkDelegate(BackgroundWorker worker, DoWorkEventArgs e, ref string errorMessage);
        // public delegate TReturn DoWork_WorkDelegate<TReturn>(BackgroundWorker worker, DoWorkEventArgs e, ref string errorMessage);

        protected FileDialogInfo<object, string> SettingsFileDialogInfo; //Note:a non-ref-returning indexer (etc etc); this cannot be a property
        private Dictionary<string, TIcon> ActionIconImages { get; }

        #endregion Declarations

        #region Constructors
        public ConsoleViewModel()
        {
            if (SettingsController<TSettings>.Settings == null)
            {
                SettingsController<TSettings>.New();
            }
        }

        public ConsoleViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, TIcon> actionIconImages,
            FileDialogInfo<object, string> settingsFileDialogInfo

        ) :
            this()
        {
            try
            {
                //(and the delegate it contains
                if (propertyChangedEventHandlerDelegate != default)
                {
                    PropertyChanged += new PropertyChangedEventHandler(propertyChangedEventHandlerDelegate);
                }

                ActionIconImages = actionIconImages;

                SettingsFileDialogInfo = settingsFileDialogInfo;

                ActionIconImage = ActionIconImages["Save"];
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
        #endregion Constructors

        #region Methods
        public void FileNew()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            MessageDialogInfo<object, string, object, string, object> messageDialogInfo = null;
            string errorMessage = null;

            try
            {
                StartProgressBar
                (
                    "New...",
                    null,
                    ActionIconImages["New"],
                    true,
                    33
                );

                if (SettingsController<TSettings>.Settings.Dirty)
                {
                    //prompt before saving
                    messageDialogInfo =
                        new MessageDialogInfo<object, string, object, string, object>
                        (
                            null,
                            true,
                            "Save changes?",
                            null,
                            "Question",
                            null,
                            "Save changes?: ",
                            ""
                        );

                    if (!Dialogs.ShowMessageDialog(ref messageDialogInfo,ref errorMessage))
                    {
                        throw new ApplicationException(errorMessage);
                    }

                    if (!string.IsNullOrWhiteSpace(messageDialogInfo.Response))
                    {
                        if (string.Equals(messageDialogInfo.Response, "Y", StringComparison.CurrentCultureIgnoreCase))
                        {
                            //SAVE
                            FileSaveAs();
                        }
                        else
                        {
                            //treat as cancel
                        }
                    }
                    else
                    {
                        //treat as cancel
                    }
                }

                //NEW
                if (!SettingsController<TSettings>.New())
                {
                    throw new ApplicationException(string.Format("Unable to get New settings.\r\nPath: {0}", SettingsController<TSettings>.FilePath));
                }

                ModelController<TModel>.Model.Refresh();

                StopProgressBar("New completed.");
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar("", string.Format("{0}", ex.Message));
            }
        }

        /// <summary>
        /// Open object at SettingsController<TSettings>.FilePath.
        /// </summary>
        /// <param name="forceDialog">If false, just use SettingsController<TSettings>.FilePath</param>
        public void FileOpen(bool forceDialog = true)
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            MessageDialogInfo<object, string, object, string, object> messageDialogInfo = null;
            string errorMessage = null;

            try
            {
                StartProgressBar
                (
                    string.Format("Opening {0} as {1}...", SettingsController<TSettings>.FilePath, SettingsBase.SerializeAs.ToString()),
                    null,
                    ActionIconImages["Open"],
                    true,
                    33
                );

                if (SettingsController<TSettings>.Settings.Dirty)
                {
                    //prompt before saving
                    messageDialogInfo =
                        new MessageDialogInfo<object, string, object, string, object>
                        (
                            null,
                            true,
                            "Save changes?",
                            null,
                            "Question",
                            null,
                            "Save changes?: ",
                            ""
                        );

                    if (!Dialogs.ShowMessageDialog(ref messageDialogInfo,ref errorMessage))
                    {
                        throw new ApplicationException(errorMessage);
                    }

                    if (!string.IsNullOrWhiteSpace(messageDialogInfo.Response))
                    {
                        if (string.Equals(messageDialogInfo.Response, "Y", StringComparison.CurrentCultureIgnoreCase))
                        {
                            //SAVE
                            FileSave();
                        }
                        else
                        {
                            //treat as cancel
                        }
                    }
                    else
                    {
                        //treat as cancel
                    }
                }

                if (forceDialog)
                {
                    SettingsFileDialogInfo.Filename = SettingsController<TSettings>.FilePath;
                    if (Dialogs.GetPathForLoad(ref SettingsFileDialogInfo, ref errorMessage))
                    {
                        SettingsController<TSettings>.FilePath = SettingsFileDialogInfo.Filename;
                    }
                    else
                    {
                        StopProgressBar("Open cancelled.");
                        return; //if open was cancelled
                    }
                }

                //OPEN
                if (!SettingsController<TSettings>.Open())
                {
                    throw new ApplicationException(string.Format("Unable to Open settings.\r\nPath: {0}", SettingsController<TSettings>.FilePath));
                }

                ModelController<TModel>.Model.Refresh();

                StopProgressBar("Opened.");
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public void FileSave()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            string errorMessage = null;

            try
            {
                StartProgressBar
                (
                    string.Format("Saving {0} as {1}...", SettingsController<TSettings>.FilePath, SettingsBase.SerializeAs.ToString()),
                    null,
                    ActionIconImages["Save"],
                    true,
                    33
                );

                SettingsFileDialogInfo.ForceDialog = false;
                //_settingsFileDialogInfo.Modal = true;
                SettingsFileDialogInfo.Filename = SettingsController<TSettings>.FilePath;
                if (Dialogs.GetPathForSave(ref SettingsFileDialogInfo, ref errorMessage))
                {
                    SettingsController<TSettings>.FilePath = SettingsFileDialogInfo.Filename;

                    //SAVE
                    if (!SettingsController<TSettings>.Save())
                    {
                        throw new ApplicationException(string.Format("Unable to Save settings.\r\nPath: {0}", SettingsController<TSettings>.FilePath));
                    }

                    ModelController<TModel>.Model.Refresh();

                    StopProgressBar("Saved completed.");
                }
                else
                {
                    //treat as cancel
                    StopProgressBar("Save cancelled.");
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public void FileSaveAs()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            string errorMessage = null;

            try
            {
                StartProgressBar
                (
                    "Saving As...",
                    null,
                    ActionIconImages["Save"],
                    true,
                    33
                );

                SettingsFileDialogInfo.Filename = SettingsController<TSettings>.FilePath;
                SettingsFileDialogInfo.ForceDialog = true;
                // _settingsFileDialogInfo.Modal = true;
                if (Dialogs.GetPathForSave(ref SettingsFileDialogInfo, ref errorMessage))
                {
                   SettingsController<TSettings>.FilePath = SettingsFileDialogInfo.Filename;

                    //SAVE
                    if (!SettingsController<TSettings>.Save())
                    {
                        throw new ApplicationException(string.Format("Unable to Save settings.\r\nPath: {0}", SettingsController<TSettings>.FilePath));
                    }

                    ModelController<TModel>.Model.Refresh();

                    StopProgressBar("Save As completed.");
                }
                else
                {
                   StopProgressBar("Save As cancelled.");
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public void FilePrint()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            PrinterDialogInfo<object, string, string> printerDialogInfo = null;
            string errorMessage = null;

            try
            {
                StartProgressBar
                (
                    "Printing...",
                    null,
                    ActionIconImages["Print"],
                    true,
                    33
                );

                //select printer
                printerDialogInfo = new PrinterDialogInfo<object, string, string>
                (
                    null,
                    true,
                    "Select Printer",
                    null
                );

                if (Dialogs.GetPrinter(ref printerDialogInfo, ref errorMessage))
                {
                    if (!string.IsNullOrWhiteSpace(printerDialogInfo.Response))
                    {
                        if (string.Equals(printerDialogInfo.Response, "OK", StringComparison.CurrentCultureIgnoreCase))
                        {
                            StatusMessage += printerDialogInfo.Printer;
                        }
                        else
                        {
                            StatusMessage += "cancelled";
                        }
                    }
                    else
                    {
                        StatusMessage += "cancelled";
                    }
                }
                else
                {
                    ErrorMessage = errorMessage;
                }

                if (Print())
                {
                    StopProgressBar("Printed.");
                }
                else
                {
                    StopProgressBar("Print cancelled.");
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("Print failed: '{0}'", ex.Message));
            }
        }

        public void FileExit()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                //Note: app will handle final cleanup

                //Application.Exit();//not applicable in demo app

                StatusMessage = string.Empty;
            }
            catch (Exception ex)
            {
                ErrorMessage = string.Format("{0}", ex.Message);

                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        public void EditCopy()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Copying...",
                    null,
                    ActionIconImages["Copy"],
                    true,
                    33
                );

                //Copy();

                StopProgressBar("Copied.");
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("Copy failed: {0}", ex.Message));
            }
        }

        public void EditProperties()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Edit Properties...",
                    null,
                    ActionIconImages["Properties"],
                    true,
                    33
                );

                ////Note:pass model, not settings, or changes will not trigger business logic in model properties
                //PropertyDialog pv = new PropertyDialog(ModelController<TModel>.Model/*SettingsController<TSettings>.Settings*/, ModelController<TModel>.Model.Refresh);
                //pv.Owner = System.Windows.Forms.Application.OpenForms[0];//App.Current.MainWindow;
                //pv.ShowDialog();

                StopProgressBar("Edit Properties closed.");
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public void HelpAbout<TAssemblyInfo>()
            where TAssemblyInfo :
            //class,
            AssemblyInfoBase<object>,
            new()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            TAssemblyInfo assemblyInfo = null;
            AboutDialogInfo<object, string, object> aboutDialogInfo = null;
			const string response = null;
            string errorMessage = null;

            try
            {
                StartProgressBar("About...", null, null, true, 33);

                assemblyInfo = new TAssemblyInfo();
                //USE text-mode About feature
                aboutDialogInfo = new AboutDialogInfo<object, string, object>()
                {
                    Parent = null,
                    Response = response,
                    Modal = true,
                    Title = "About...",
                    ProgramName =  assemblyInfo.Title,//"MvcConsole.Core",
                    Version = assemblyInfo.Version,//"v0.6",
                    Copyright = assemblyInfo.Copyright,//"Copyright (C) 1989, 1991 Free Software Foundation, Inc.  \n59 Temple Place - Suite 330, Boston, MA  02111-1307, USA",
                    Comments = assemblyInfo.Description,//"Desktop console app prototype, on Linux, in C# / DotNet[5|6], using VSCode.",
                    Website = assemblyInfo.Website,//"https://gitlab.com/sjsepan/MvcConsole.Core",
                    Logo = null
                };

                if (!Dialogs.ShowAboutDialog(ref aboutDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(errorMessage);
                }

                StopProgressBar("About completed.");
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        private static bool Print()
        {
            bool returnValue = default;

            // System.Windows.Forms.PrintDialog printDialog = new System.Windows.Forms.PrintDialog();
            // bool? dialogResult = (printDialog.ShowDialog() == DialogResult.OK);

            // if (dialogResult.HasValue && dialogResult.Value)
            // {
                try
                {
                    //printDialog.PrintVisual(/*_view.ChartControl*/, "app_name");

                    returnValue = true;
                }
                catch (Exception ex)
                {
                    Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                }
            // }

            return returnValue;
        }
        #endregion Methods

    }
}
